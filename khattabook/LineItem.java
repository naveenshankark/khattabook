package khattabook;
public class LineItem {
    
private int productId;
private int quantity;
private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public LineItem(int productId, int quantity,String Name) {
        this.productId = productId;
        this.quantity = quantity;
        this.productName=Name;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }
    
    

}
