package khattabook;
public class Product {
  private String productName;
   private int  productId;
   private int productQty;
   private double productBuyingPrice;
   private double productSellingPrice;
   private int totalQty;
   
   
Product(int productId,String productName, int productQty,double productBuyingPrice,double productSellingPrice) {
    this.productId=productId;
    this.productName=productName;
    this.productBuyingPrice=productBuyingPrice;
    this.productQty=productQty;
    this.productSellingPrice= productSellingPrice;
    this.totalQty=productQty;
    
}  

    public String getProductName() {
        return productName;
    }

    public int getProductId() {
        return productId;
    }

    public int getProductQty() {
        return productQty;
    }

    public double getProductPrice() {
        return productBuyingPrice;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }

    public double getProductSellingPrice() {
        return productSellingPrice;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public double getProductBuyingPrice() {
        return productBuyingPrice;
    }
    
    

    @Override
    public String toString() {
        return "Product{" + "productName=" + productName + ", productId=" + productId + ", productQty=" + productQty + ", productBuyingPrice=" + productBuyingPrice + ", productSellingPrice=" + productSellingPrice + '}';
    }
    


    
    
}
