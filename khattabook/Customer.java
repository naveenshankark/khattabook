
package khattabook;

public class Customer {
    
   public  String customerName;
   public  String contact;  
   public int CustomerId;
   private double debet;
    Customer(String customerName ,String contact,int CustomerId){
        this.contact=contact;
        this.customerName=customerName;
        this.CustomerId=CustomerId;
        
    }
    public String getContact(){
        return this.contact;
    }

    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName ){
         this.customerName=customerName;
    }
    public void Setcontact(String contact){
        this.contact=contact;
    }
    public int getCustomerId(){
        return this.CustomerId;
    }

    public void setDebet(double debet) {
        this.debet = debet;
    }

    public double getDebet() {
        return debet;
    }

    @Override
    public String toString() {
        return "Customer{" + "customerName=" + customerName + ", contact=" + contact + ", CustomerId=" + CustomerId + '}';
    }
    
    
            
}
